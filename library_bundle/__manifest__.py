{
    'name': 'Library_bundle',
    'version': '1.0.0',
    'summary': 'Bundle para todos los modulos de la libreria',
    'author': 'Abel Martinez',
    'website': 'https://www.netkia.es',
    'depends': [
        'custom',
        'library_member'
    ],
    'application': True,
    'auto_install': True,
}