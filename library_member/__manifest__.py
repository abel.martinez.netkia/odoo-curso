{
    'name': 'Library_menbers',
    'version': '1.0.0',
    'summary': 'Los miembros de una libreria',
    'author': 'Abel Martinez',
    'website': 'https://www.netkia.es',
    'depends': [
        'custom', 'mail'
    ],
    'data' : [
        'views/book_view.xml',
        'security/ir.model.access.csv',
        'views/library_menu.xml',
        'views/member_view.xml',
        'security/library_security.xml',
        'views/book_list_template.xml',

    ],
    'application': False,
    'auto_install': False,
}