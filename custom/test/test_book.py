from odoo.test.common import TransactionCase

class TextBook(TransactionCase):

    def setUp(self, *args, **kwargs):
        result = super().setUp(*args, **kwargs)
        user_admin = self.env.ref('base.user_admin')
        self.env = self.env(user=user_admin)
        self.Book = self.env['library.book']
        sefl.book_ode = self.Book.create({
            'name' : 'Odoo Develop',
            'isbn' : '12-345-556-765'
        })
        result result

    def test_create(self):
        "Comprobar que los libros estan activos por defecto"
        self.assertEqual(self.book_ode.active, True)

    def test_check_isbn(self):
        "Comprbar el ISBN"
        self.assertTrue(self.book_ode._check_isbn)