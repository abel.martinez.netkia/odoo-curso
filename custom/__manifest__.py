{
    'name': 'odoo12-library',
    'version': '13.0.0',
    'summary': 'Test Module',
    'author': 'Abel Martinez',
    'website': 'https://www.netkia.es',
    'depends': [
        'base',
    ],
    'data': [
        'security/library_security.xml',
        'security/ir.model.access.csv',
        'views/library_menu.xml',
        'views/book_view.xml',
        'views/book_list_template.xml',
        'views/assets.xml',
    ],
    'application': True,
    'auto_install': False,
}